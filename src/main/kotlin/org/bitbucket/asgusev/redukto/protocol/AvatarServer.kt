package org.bitbucket.asgusev.redukto.protocol

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousCloseException
import java.nio.channels.AsynchronousServerSocketChannel
import java.nio.channels.AsynchronousSocketChannel
import java.nio.channels.CompletionHandler


class AvatarServer(image: ByteArray, private val port: Int = DEFAULT_PORT) {
    companion object {
        const val DEFAULT_PORT = 4645
    }

    private var serverSocket: AsynchronousServerSocketChannel? = null
    private val response = (
            "HTTP/1.0 200 OK\r\n" +
                    "Content-Type: image/png\r\n" +
                    "Content-Length: ${image.size}\r\n\r\n"
            ).toByteArray() +
            image
    private val responseBuffer = ByteBuffer.allocate(response.size)
        .apply {
            put(response)
            flip()
        }

    fun start() {
        serverSocket = AsynchronousServerSocketChannel.open()
        serverSocket?.bind(InetSocketAddress(port))
        serverSocket?.accept(null, connectionHandler)
    }

    private val connectionHandler = object: CompletionHandler<AsynchronousSocketChannel, Nothing?> {
        override fun completed(p0: AsynchronousSocketChannel?, p1: Nothing?) {
            while (responseBuffer.hasRemaining())
                p0?.write(responseBuffer)?.get()
            responseBuffer.rewind()
            serverSocket?.accept(null, this)
        }

        override fun failed(error: Throwable?, p1: Nothing?) {
            if (error !is AsynchronousCloseException)
                serverSocket!!.accept(null, this)
        }
    }

    fun stop() {
        serverSocket?.close()
        serverSocket = null
    }
}
