package org.bitbucket.asgusev.redukto.protocol

import java.net.InetAddress
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousByteChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.streams.toList


fun String.toByteArrayNullTerm() = toByteArray() + 0.toByte()


const val BUFFER_CAPACITY = 1 shl 22
const val HEADER_BUFFER_CAPACITY = 1 shl 12


fun ByteBuffer.getLongLE(): Long {
    var res = 0L
    for (shift in 0 .. 56 step 8) {
        val curByte = this.get().toLong() and 255L
        res = res or (curByte shl shift)
    }
    return res
}


fun ByteBuffer.putLongLE(value: Long) {
    for (shift in 0 .. 56 step 8)
        this.put((value shr shift).toByte())
}


fun readByte(buffer: ByteBuffer, channel: AsynchronousByteChannel): Byte {
    while (!buffer.hasRemaining()) {
        buffer.clear()
        channel.read(buffer).get()
        buffer.flip()
    }
    return buffer.get()
}


fun readNullTermString(channel: AsynchronousByteChannel, buffer: ByteBuffer): String {
    val stringBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
    var c: Byte
    do {
        c = readByte(buffer, channel)
        stringBuffer.put(c)
    } while (c.toInt() != 0)
    return String(Arrays.copyOf(stringBuffer.array(), stringBuffer.position() - 1))
}


fun readStringByBytesNumber(channel: AsynchronousByteChannel, buffer: ByteBuffer, bytesNumber: Long): String {
    val stringBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
    for (i in 1..bytesNumber)
        stringBuffer.put(readByte(buffer, channel))
    stringBuffer.put(0.toByte())
    return String(Arrays.copyOf(stringBuffer.array(), stringBuffer.position() - 1))
}


class PathCollisionFixer(private val root: Path) {
    private val dirsMap = mutableMapOf<String, String>()

    private fun addNumberToName(name: String, suffixNumber: Int): String {
        val pointIndex = name.lastIndexOf(".")
        if (pointIndex < 0)
            return "$name ($suffixNumber)"
        val pureName = name.substring(0, pointIndex)
        val extension = name.substring(pointIndex)
        return "$pureName ($suffixNumber)$extension"
    }

    private fun findFreeName(name: String): String {
        if (!Files.exists(root.resolve(name)))
            return name
        var suffixNumber = 0
        var freeName: String?
        do  {
            ++suffixNumber
            freeName = addNumberToName(name, suffixNumber)
        } while (Files.exists(root.resolve(freeName!!)))
        return freeName
    }

    fun fixPath(path: Path): Path {
        val firstPart = path.first().toString()
        return if (path.nameCount == 1)
            root.resolve(findFreeName(firstPart))
        else
            root.resolve(dirsMap[firstPart]!!).resolve(path.subpath(1, path.nameCount))
    }

    fun declareDir(name: String) {
        dirsMap[name] = findFreeName(name)
    }
}


fun readDirectory(dirPath: Path, prefix: Path = Paths.get("")): List<TransferItem> {
    val childPrefix = prefix.resolve(dirPath.fileName)
    val dirItem = DirectoryItem(childPrefix)
    val childrenItems = Files.list(dirPath).toList().flatMap {
        if (Files.isDirectory(it))
            readDirectory(it, childPrefix)
        else
            listOf(FileItem(it, childPrefix))
    }
    return listOf(dirItem) + childrenItems
}

fun shortenName(fullName: String): String {
    val bracePos = fullName.lastIndexOf(" (")
    return fullName.substring(0, bracePos)
}

fun authorRepresentation(authorName: String?, authorIP: InetAddress) =
    authorName?.let(::shortenName) ?: authorIP.toString()
