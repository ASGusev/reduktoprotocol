package org.bitbucket.asgusev.redukto.protocol

import java.io.ByteArrayInputStream
import java.io.IOException
import java.lang.Integer.min
import java.net.*
import java.nio.ByteBuffer
import java.nio.channels.*
import java.nio.file.*
import java.util.concurrent.ExecutionException


private const val CODE_HELLO_BROAD: Byte       = 1
private const val CODE_HELLO_SINGLE: Byte      = 2
private const val CODE_REMOVE: Byte            = 3
private const val CODE_HELLO_PORT_BROAD: Byte  = 4
private const val CODE_HELLO_PORT_SINGLE: Byte = 5
private val CODES_HELLO = setOf(
    CODE_HELLO_BROAD, CODE_HELLO_SINGLE, CODE_HELLO_PORT_BROAD, CODE_HELLO_PORT_SINGLE
)
private const val GOODBYE_MESSAGE = "Bye Bye"
private const val TEXT_HEADER = "___DUKTO___TEXT___"
private const val FILE_SIZE_DIRECTORY = -1L


data class Peer(val address: InetAddress, val port: Short, val name: String) {
    override fun equals(other: Any?) = (other as Peer).address == address
    override fun hashCode() = address.hashCode()
}


interface SendingListener {
    fun onProgress(bytesSent: Long, totalBytes: Long) {}
    fun onFinish() {}
    fun onError(e: Exception?) {}
}

interface ReceptionListener {
    fun onProgress(newBytes: ByteBuffer, bytesReceived: Long, totalBytes: Long) {}
    fun onFinish() {}
    fun onError(e: Exception?) {}
}

interface ProtocolListener {
    fun onNewPeer(peer: Peer) {}
    fun onDirectoryDeclaration(name: String, authorIP: InetAddress, authorName: String?) {}
    fun onFileReception(fileName: String, fileSize: Long, authorIP: InetAddress, authorName: String?):
            ReceptionListener? = null
    fun onMessageReceived(message: String, authorIP: InetAddress, authorName: String?) {}
    fun onLeftPeer(peer: Peer) {}
    fun onError(e: Throwable?) {}
}


interface TransferItem {
    val size: Long
    val header: String
    fun getChannel(): ReadableByteChannel
}


class TextItem(text: String): TransferItem {
    private val textBytes = text.toByteArray()

    override val size = textBytes.size.toLong()
    override val header = TEXT_HEADER
    override fun getChannel(): ReadableByteChannel = Channels.newChannel(textBytes.inputStream())
}


class FileItem(private val path: Path, prefix: Path = Paths.get("")): TransferItem {
    override val size = Files.size(path)
    override val header = prefix.resolve(path.fileName).joinToString("/")
    override fun getChannel(): ReadableByteChannel = FileChannel.open(path, StandardOpenOption.READ)
}


class DirectoryItem(path: Path): TransferItem {
    override val header = path.joinToString("/")
    override val size = FILE_SIZE_DIRECTORY
    override fun getChannel(): ReadableByteChannel = Channels.newChannel(ByteArrayInputStream(byteArrayOf()))
}


class DuktoProtocol(private val port: Short = DEFAULT_PORT, private val listener: ProtocolListener? = null,
                    userName: String? = null, deviceName: String? = null, osName: String? = null,
                    networkInterfaces: List<NetworkInterface>? = null) {
    companion object {
        const val DEFAULT_PORT: Short = 4644
        private val SKIP_ADDRESSES = listOf(InetAddress.getByName("0.0.0.0"))
    }

    private val userName: String = userName ?: System.getProperty("user.name")!!.replace(' ', '-')
    private val deviceName: String = (deviceName ?: InetAddress.getLocalHost().hostName).replace(' ', '-')
    private val osName: String = osName ?: System.getProperty("os.name")!!.replace(' ', '-')
    val signature = "${this.userName} at ${this.deviceName} (${this.osName})"

    private val udpSocket = DatagramSocket(port.toInt())
    private val broadcastAddressList: List<InetAddress>
    val localAddresses: List<InetAddress>

    init {
        val usableNetworkInterfaces = networkInterfaces ?: NetworkInterface.getNetworkInterfaces()
            .toList()
            .filter { it.isUp && !it.isLoopback && !it.isVirtual }
            .filter { networkInterface ->
                networkInterface.interfaceAddresses
                    .any { it.broadcast != null && it.address !in SKIP_ADDRESSES }
            }

        broadcastAddressList = usableNetworkInterfaces
            .flatMap { it.interfaceAddresses }
            .mapNotNull { it.broadcast }
            .filter { it !in SKIP_ADDRESSES }
            .toList()

        localAddresses = usableNetworkInterfaces
            .flatMap { it.inetAddresses.toList() }
            .filter {
                !it.isLoopbackAddress && !it.isAnyLocalAddress && !it.isLinkLocalAddress && !it.isMulticastAddress &&
                        it !in SKIP_ADDRESSES
            }.toList()
    }

    val peers = mutableSetOf<Peer>()
    private val tcpServer = AsynchronousServerSocketChannel.open()
    private val listeningThread: Thread
    @Volatile private var running = true

    private val tcpMessageHandler = object: CompletionHandler<AsynchronousSocketChannel, Nothing?> {
        override fun failed(error: Throwable?, p1: Nothing?) {
            if (error !is AsynchronousCloseException && running) {
                listener?.onError(error)
                tcpServer.accept(null, this)
            }
        }

        override fun completed(sourceChannel: AsynchronousSocketChannel?, p1: Nothing?) {
            val msgBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
            val nFiles: Long
            try {
                while (msgBuffer.position() < 2 * Long.SIZE_BYTES)
                    sourceChannel?.read(msgBuffer)?.get()
                msgBuffer.flip()
                nFiles = msgBuffer.getLongLE()
                msgBuffer.getLongLE()  // Total size
            } catch (e: ExecutionException) {
                listener?.onError(e.cause)
                tcpServer.accept(null, this)
                return
            }
            itemLoop@ for (i in 1 .. nFiles) {
                lateinit var fileName: String
                try {
                    fileName = readNullTermString(sourceChannel!!, msgBuffer)
                } catch (e: IOException) {
                    listener?.onError(e)
                    break
                }
                val fileSize = msgBuffer.getLongLE()

                val sourceAddress = InetAddress.getByName(
                    sourceChannel.remoteAddress.toString().takeWhile { it != ':' }.substring(1))
                val author = peers.find { it.address == sourceAddress }

                when {
                    fileSize == FILE_SIZE_DIRECTORY ->
                        listener?.onDirectoryDeclaration(fileName, sourceAddress, author?.name)
                    fileName == TEXT_HEADER ->
                        if (!acceptTextMessage(sourceChannel, msgBuffer, fileSize, sourceAddress, author))
                            break@itemLoop
                    else ->
                        if (!acceptFile(fileName, fileSize, sourceAddress, author, msgBuffer, sourceChannel))
                            break@itemLoop
                }
            }
            tcpServer.accept(null, this)
        }

        private fun acceptFile(fileName: String, fileSize: Long,
                               sourceAddress: InetAddress, author: Peer?,
                               msgBuffer: ByteBuffer, sourceChannel: AsynchronousSocketChannel): Boolean {
            val fileListener = listener?.onFileReception(fileName, fileSize, sourceAddress, author?.name)
            try {
                var bytesReceived = msgBuffer.remaining().toLong()
                if (bytesReceived > fileSize) {
                    val curFileBuffer = msgBuffer.slice()
                    curFileBuffer.limit(fileSize.toInt())
                    bytesReceived = fileSize
                    fileListener?.onProgress(curFileBuffer, bytesReceived, fileSize)
                    msgBuffer.position(msgBuffer.position() + fileSize.toInt())
                } else {
                    fileListener?.onProgress(msgBuffer, bytesReceived, fileSize)
                }
                while (bytesReceived < fileSize) {
                    msgBuffer.clear()
                    msgBuffer.limit(min((fileSize - bytesReceived).toInt(), msgBuffer.capacity()))
                    sourceChannel.read(msgBuffer)?.get()
                    val read = msgBuffer.position()
                    msgBuffer.flip()
                    bytesReceived += read
                    fileListener?.onProgress(msgBuffer, bytesReceived, fileSize)
                }
                fileListener?.onFinish()
            } catch (e: IOException) {
                listener?.onError(e)
                return false
            } catch (e: ExecutionException) {
                listener?.onError(e.cause)
                return false
            }
            return true
        }

        private fun acceptTextMessage(sourceChannel: AsynchronousSocketChannel,
                                      msgBuffer: ByteBuffer, fileSize: Long,
                                      sourceAddress: InetAddress, author: Peer?): Boolean {
            try {
                val message = readStringByBytesNumber(sourceChannel, msgBuffer, fileSize)
                listener?.onMessageReceived(message, sourceAddress, author?.name)
            } catch (e: IOException) {
                listener?.onError(e)
                return false
            } catch (e: ExecutionException) {
                listener?.onError(e.cause)
                return false
            }
            return true
        }
    }

    init {
        udpSocket.soTimeout = 1000
        udpSocket.broadcast = true
        listeningThread = Thread { listenToAddresses() }
        listeningThread.start()
        tcpServer.bind(InetSocketAddress(port.toInt()))
        tcpServer.accept(null, tcpMessageHandler)
        sayHelloBroad()
    }

    private fun broadcast(data: ByteBuffer) {
        for (address in broadcastAddressList) {
            val packet = DatagramPacket(data.array(), data.position(), address, port.toInt())
            udpSocket.send(packet)
        }
    }

    private fun sayHelloSingle(peer: Peer) {
        val msgBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
        msgBuffer.put(CODE_HELLO_SINGLE)
        msgBuffer.put(signature.toByteArray())
        val pack = DatagramPacket(msgBuffer.array(), msgBuffer.position(), peer.address, peer.port.toInt())
        udpSocket.send(pack)
    }

    fun sayHelloBroad() {
        val msgBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
        msgBuffer.put(CODE_HELLO_BROAD)
        msgBuffer.put(signature.toByteArray())
        broadcast(msgBuffer)
    }

    fun sayGoodbye() {
        val msgBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
        msgBuffer.put(CODE_REMOVE)
        msgBuffer.put(GOODBYE_MESSAGE.toByteArray())
        for (peer in peers)
            udpSocket.send(DatagramPacket(msgBuffer.array(), msgBuffer.position(), peer.address, peer.port.toInt()))
    }

    private fun listenToAddresses() {
        while (running) {
            val responseBuffer = ByteArray(HEADER_BUFFER_CAPACITY)
            val resp = DatagramPacket(responseBuffer, HEADER_BUFFER_CAPACITY)
            try {
                udpSocket.receive(resp)
                val sourceAddress = InetAddress.getByName(resp.socketAddress.toString()
                    .takeWhile { it != ':' }
                    .substring(1)
                )
                if (sourceAddress !in localAddresses) {
                    when (val messageTypeCode = responseBuffer[0]) {
                        in CODES_HELLO -> {
                            val peerPort: Short
                            val peerSignatureStart: Int
                            if (messageTypeCode == CODE_HELLO_BROAD || messageTypeCode == CODE_HELLO_SINGLE) {
                                peerPort = DEFAULT_PORT
                                peerSignatureStart = 1
                            } else {
                                peerPort = (responseBuffer[1] + responseBuffer[2] * 256).toShort()
                                peerSignatureStart = 3
                            }
                            val peerSignature = String(responseBuffer.copyOfRange(peerSignatureStart, resp.length))
                            val newPeer = Peer(sourceAddress, peerPort, peerSignature)
                            if (messageTypeCode == CODE_HELLO_BROAD || messageTypeCode == CODE_HELLO_PORT_BROAD)
                                sayHelloSingle(newPeer)

                            val peerIsNew = peers.add(newPeer)
                            if (peerIsNew)
                                listener?.onNewPeer(newPeer)
                        }
                        CODE_REMOVE -> {
                            val peerToRemove = peers.find { it.address == sourceAddress }
                            peers.remove(peerToRemove)
                            if (peerToRemove != null)
                                listener?.onLeftPeer(peerToRemove)
                        }
                    }
                }
            }
            catch (e: SocketTimeoutException) {}
            catch (e: SocketException) {}
        }
    }

    fun stop() {
        sayGoodbye()
        tcpServer.close()
        running = false
        udpSocket.close()
        listeningThread.join()
    }

    fun send(items: List<TransferItem>, target: String, port: Short = DEFAULT_PORT,
             sendingListener: SendingListener? = null) {
        val headerBuffer = ByteBuffer.allocate(2 * Long.SIZE_BYTES)
        val totalSize = items.map { it.size }.sum()
        headerBuffer.putLongLE(items.size.toLong())
        headerBuffer.putLongLE(totalSize)
        headerBuffer.flip()

        try {
            var totalBytesSent = 0L
            val targetChannel = SocketChannel.open(InetSocketAddress(target, port.toInt()))
            targetChannel.use {
                targetChannel.write(headerBuffer)
                for (transferItem in items) {
                    val dataBuffer = ByteBuffer.allocate(BUFFER_CAPACITY)
                    dataBuffer.put(transferItem.header.toByteArrayNullTerm())
                    dataBuffer.putLongLE(transferItem.size)

                    val dataChannel = transferItem.getChannel()
                    dataChannel.use {
                        var bytesSent = 0L
                        do {
                            dataChannel.read(dataBuffer)
                            val read = dataBuffer.position()
                            dataBuffer.flip()
                            targetChannel.write(dataBuffer)
                            dataBuffer.clear()
                            bytesSent += read
                            totalBytesSent += read
                            sendingListener?.onProgress(totalBytesSent, totalSize)
                        } while (read > 0)
                    }
                }
            }
            sendingListener?.onFinish()
        } catch (e: IOException ) {
            sendingListener?.onError(e)
        } catch (e: UnresolvedAddressException) {
            sendingListener?.onError(e)
        }
    }

    fun restart() {
        sayGoodbye()
        peers.clear()
        sayHelloBroad()
    }
}
